# README #

This is Tet test task solution by Olegs Kurovs.

Please note: I have committed .env file for laravel app, just so you don't have to modify it manually in order to launch the app.

### How do I get set up? ###

First, clone this repository to your local machine:

```
git clone git@bitbucket.org:okurovs/majasdarbs.git
cd majasdarbs
```

Then, run docker compose:

```
docker-compose up -d
```

That's it!

Client side is available on http://localhost:80

Server side API is available on http://localhost:81/api/currencies

### Cron-job to check for new data ###

Cron-job script is available as Laravel Artisan command called "check:feed".

In order to run it from your local machine:

```
docker-compose exec php-fpm php artisan check:feed
```