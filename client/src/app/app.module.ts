import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { CurrencyListComponent } from './currency-list/currency-list.component';
import { ExpandedCurrencyViewComponent } from './expanded-currency-view/expanded-currency-view.component';

@NgModule({
  declarations: [
    AppComponent,
    CurrencyListComponent,
    ExpandedCurrencyViewComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
