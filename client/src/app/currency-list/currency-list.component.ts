import { Component, OnInit } from '@angular/core';
import { CurrencyService, Currency } from '../currency.service';

@Component({
  selector: 'app-currency-list',
  templateUrl: './currency-list.component.html',
  styleUrls: ['./currency-list.component.css']
})
export class CurrencyListComponent implements OnInit {
  public currentPage: number = 1;
  public maxPageNumber = 1;
  public shownCurrencies: Currency[];
  public pageNumbers: number[];

  private currencyList: Currency[];
  private pageSize = 10;

  constructor(
    private currencyService: CurrencyService
  ) { }

  ngOnInit(): void {
    this.currencyService.getCurrencyList()
      .subscribe(this.onCurrencyListReceived.bind(this));
  }

  public nextPage() {
    this.onPageChanged(this.currentPage + 1);
  }

  public previousPage() {
    this.onPageChanged(this.currentPage - 1);
  }

  public onPageChanged(pageNumber: number) {
    if (pageNumber >= 1 && pageNumber <= this.maxPageNumber) {
      this.currentPage = pageNumber;
      this.updateShownCurrencies();
    }
  }

  public onCurrencyClicked(currencyName: string) {
    this.currencyService.getFullCurrencyData(currencyName);
  }

  private onCurrencyListReceived(currencyList: Currency[]): void {
    this.currencyList = currencyList;
    this.maxPageNumber = Math.ceil(this.currencyList.length / this.pageSize);
    this.updateShownCurrencies();

    let possiblePageNumbers = [];
    for (let i = 1; i <= this.maxPageNumber; i++) {
      possiblePageNumbers.push(i);
    }
    this.pageNumbers = possiblePageNumbers;
  }

  private updateShownCurrencies(): void {
    const startIndex = (this.currentPage - 1) * this.pageSize;
    const endIndex = startIndex + this.pageSize;

    this.shownCurrencies = this.currencyList.slice(startIndex, endIndex);
  }
}
