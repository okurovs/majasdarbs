import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

export interface Currency {
  currencyName: string,
  exchangeRate: number
}

export interface FullCurrencyData {
  currencyName?: string,
  exchangeRates: { rate: number, date: string }[]
}

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {
  private fullCurrencyDataSubject: Subject<FullCurrencyData> = new Subject<FullCurrencyData>();
  public fullCurrencyDataObservable: Observable<FullCurrencyData> = this.fullCurrencyDataSubject.asObservable();

  private baseUrl = 'http://localhost:81/api/currencies/';

  constructor(
    private http: HttpClient
  ) { }

  getCurrencyList(): Observable<Currency[]> {
    return this.http.get<Currency[]>(this.baseUrl);
  }

  getFullCurrencyData(currencyName: string): void {
    this.http.get(this.baseUrl + currencyName)
      .subscribe((data: FullCurrencyData) => {
        this.fullCurrencyDataSubject.next(data);
      });
  }
}
