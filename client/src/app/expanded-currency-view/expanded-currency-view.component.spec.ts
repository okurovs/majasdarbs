import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpandedCurrencyViewComponent } from './expanded-currency-view.component';

describe('ExpandedCurrencyViewComponent', () => {
  let component: ExpandedCurrencyViewComponent;
  let fixture: ComponentFixture<ExpandedCurrencyViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExpandedCurrencyViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpandedCurrencyViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
