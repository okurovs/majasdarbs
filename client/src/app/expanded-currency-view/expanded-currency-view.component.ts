import { Component, OnInit } from '@angular/core';
import { CurrencyService, FullCurrencyData } from '../currency.service';

@Component({
  selector: 'app-expanded-currency-view',
  templateUrl: './expanded-currency-view.component.html',
  styleUrls: ['./expanded-currency-view.component.css']
})
export class ExpandedCurrencyViewComponent implements OnInit {
  public currencyData: FullCurrencyData;

  constructor(
    private currencyService: CurrencyService
  ) { }

  ngOnInit(): void {
    this.currencyService.fullCurrencyDataObservable
      .subscribe(this.onNewCurrencyDataReceived.bind(this));
  }

  private onNewCurrencyDataReceived(data: FullCurrencyData) {
    this.currencyData = data;
  }
}
