<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Currency;
use SimpleXMLElement;

class CheckRssFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:feed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check www.bank.lv RSS feed for recent exchange rates and save new ones in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = Http::get('https://www.bank.lv/vk/ecb_rss.xml');

        if ($response->failed()) {
            $this->info('Request failed. Please try again later.');
            return 0;
        }

        $data = new SimpleXMLElement($response->body());

        foreach ($data->channel->item as $item) {
            $allCurrencies = \App\Models\Currency::all();
            $date = date('Y-m-d H:i:s', strtotime($item->pubDate));

            $description = (string) $item->description;
            $elements = array_filter(explode(' ', $description));

            while (!empty($elements)) {
                $currencyName = array_shift($elements);
                $exchangeRate = floatval(array_shift($elements));

                $currency = $allCurrencies->firstWhere('name', $currencyName);

                if ($currency === null) {
                    $currency = $this->addNewCurrency($currencyName);
                }

                $currency->exchangeRates()->firstOrCreate([
                    'date' => $date,
                ], [
                    'rate' => $exchangeRate
                ]);
            }
        }

        return 0;
    }

    private function addNewCurrency(string $currencyName)
    {
        $currency = new Currency;
        $currency->name = $currencyName;
        $currency->save();

        return $currency;
    }
}
