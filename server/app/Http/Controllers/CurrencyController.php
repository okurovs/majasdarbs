<?php

namespace App\Http\Controllers;

use App\Models\Currency;

class CurrencyController extends Controller
{
    public function listAll()
    {
        return Currency::all()
            ->map(function ($currency) {
                $currentExchangeRate = $currency->exchangeRates()->orderByDesc('date')->first();

                return [
                    'currencyName' => $currency->name,
                    'exchangeRate' => $currentExchangeRate->rate
                ];
            }
        );
    }

    public function expandOne(string $currencyName)
    {
        $currency = Currency::where('name', $currencyName)->first();

        if ($currency === null) {
            return null;
        }

        return [
            'currencyName' => $currencyName,
            'exchangeRates' => $currency->exchangeRates()->get()
        ];
    }
}
